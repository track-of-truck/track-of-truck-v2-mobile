import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'app-language',
  templateUrl: './language.page.html',
  styleUrls: ['./language.page.scss'],
})
export class LanguagePage implements OnInit {

  constructor(private modalController: ModalController, private userService: UserService) { }

  ngOnInit() {
  }

  dismissModal() {
    this.modalController.dismiss();
  }

  async selectLanguage(languageCode: string) {
    await this.userService.setUserPreferredLanguage(languageCode);
    this.dismissModal();
  }

}
