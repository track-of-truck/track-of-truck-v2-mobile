import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ScriptInjectorService } from 'src/app/core/script-injector.service';
import { from, of, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UserService } from 'src/app/core/user.service';
import { environment } from 'src/environments/environment';
import { GeoPoint } from 'src/app/models/geo-point';
import { LoggerService } from 'src/app/core/logger.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('mapContainer', { static: false })
  mapContainer: ElementRef;

  subscriptions: Subscription[] = [];
  loading = true;
  updating = false;

  map: google.maps.Map;


  constructor(
    private modalController: ModalController,
    private userService: UserService,
    private scriptInjector: ScriptInjectorService,
    private geolocation: Geolocation,
    private logger: LoggerService,
    private toastController: ToastController,
    private translate: TranslateService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.subscriptions.push(
      this.scriptInjector.loadScript(`https://maps.googleapis.com/maps/api/js?key=${environment.googleMapsAPIKey}&libraries=geometry`).pipe(
        switchMap(() => {
          return from(this.userService.getUserLocation());
        }),
        switchMap((location) => {
          if (location) {
            return of({ location, residentialLocation: true });
          }
          this.logger.info('Getting user\'s current location');
          return from(this.geolocation.getCurrentPosition({ enableHighAccuracy: false })).pipe(
            map(locationData => {
              const { latitude, longitude } = locationData.coords;
              const geoPoint: GeoPoint = { latitude, longitude };
              this.logger.info('Got user\'s location');
              return { location: geoPoint, residentialLocation: false };
            }),
          );
        }),
      ).subscribe(async ({ location, residentialLocation }) => {
        this.initializeMap(location);
        this.loading = false;
        if (!residentialLocation) {
          // show the notification
          this.logger.info('Showing the toast message');
          const message = await this.translate.get('settings.select_location.residential_location_not_available').toPromise();
          const toast = await this.toastController.create({
            message,
            duration: 3000,
            position: 'top'
          });
          await toast.present();
        }
      })
    );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  dismissModal() {
    this.modalController.dismiss();
  }

  initializeMap(location: GeoPoint) {
    const center = new google.maps.LatLng(location.latitude, location.longitude);

    this.map = new google.maps.Map(this.mapContainer.nativeElement, {
      center,
      streetViewControl: false,
      mapTypeControl: false,
      clickableIcons: false,
      rotateControl: false,
      fullscreenControl: false,
      minZoom: 12,
      zoom: 15,
      zoomControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT,
      }
    });
  }

  async updateResidentialLocation() {
    this.logger.info('Updating residential location');
    const location: GeoPoint = {
      latitude: this.map.getCenter().lat(),
      longitude: this.map.getCenter().lng(),
    };

    this.updating = true;
    await this.userService.setResidentialLocation(location);
    this.updating = false;
    this.dismissModal();
  }

}
