import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { LoggerService } from 'src/app/core/logger.service';
import { UserService } from 'src/app/core/user.service';
import { LanguagePage } from './language/language.page';
import { LocationPage } from './location/location.page';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit, OnDestroy {

  notifications = true;
  loading = true;
  subscriptions: Subscription[] = [];

  constructor(
    private modalController: ModalController,
    private userService: UserService,
    private logger: LoggerService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.userService.getUserNotificationPreference().then(notifications => {
      this.notifications = notifications;
      this.loading = false;
    });

    this.subscriptions.push(
      this.route.queryParamMap.subscribe(qpm => {
        const setLocationParam = qpm.get('setLocation');
        if (setLocationParam && setLocationParam === 'true') {
          this.logger.info('Set the location of user');
          this.showLocationSelectionModal();
        }
      }),
    );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  async updateNotificationPreference() {
    this.logger.info('Updating notification preference', this.notifications);
    await this.userService.setUserNotificationPreference(this.notifications);
  }

  async showLanguageSelectionModal() {
    const modal = await this.modalController.create({
      component: LanguagePage,
    });
    await modal.present();
  }

  async showLocationSelectionModal() {
    const modal = await this.modalController.create({
      component: LocationPage,
    });
    await modal.present();

    // wait till modal gets dismissed
    await modal.onDidDismiss();
    this.logger.info('Modal dismissed');
    this.router.navigate(['/tabs/settings'], { queryParams: { setLocation: false } });
  }
}
