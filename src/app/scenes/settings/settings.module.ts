import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingsPageRoutingModule } from './settings-routing.module';

import { SettingsPage } from './settings.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { LanguagePageModule } from './language/language.module';
import { LocationPageModule } from './location/location.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingsPageRoutingModule,
    SharedModule,
    LanguagePageModule,
    LocationPageModule,
  ],
  declarations: [SettingsPage]
})
export class SettingsPageModule {}
