import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScenesRoutingModule } from './scenes-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ScenesRoutingModule
  ]
})
export class ScenesModule { }
