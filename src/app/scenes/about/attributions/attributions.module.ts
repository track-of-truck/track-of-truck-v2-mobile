import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttributionsPageRoutingModule } from './attributions-routing.module';

import { AttributionsPage } from './attributions.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttributionsPageRoutingModule,
    SharedModule,
  ],
  declarations: [AttributionsPage]
})
export class AttributionsPageModule {}
