import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevelopersPageRoutingModule } from './developers-routing.module';

import { DevelopersPage } from './developers.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DevelopersPageRoutingModule,
    SharedModule,
  ],
  declarations: [DevelopersPage]
})
export class DevelopersPageModule {}
