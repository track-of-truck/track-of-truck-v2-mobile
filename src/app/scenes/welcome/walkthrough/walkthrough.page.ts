import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'app-walkthrough',
  templateUrl: './walkthrough.page.html',
  styleUrls: ['./walkthrough.page.scss'],
})
export class WalkthroughPage implements OnInit {

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  async completeWalkthrough() {
    const location = await this.userService.getUserLocation();
    this.router.navigate(['/', 'tabs', 'settings'], { queryParams: { setLocation: !!!location } });
  }

}
