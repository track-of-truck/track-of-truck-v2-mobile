import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'app-select-language',
  templateUrl: './select-language.page.html',
  styleUrls: ['./select-language.page.scss'],
})
export class SelectLanguagePage implements OnInit {

  currentLanguage: string;

  constructor(private router: Router, private translate: TranslateService, private userService: UserService) { }

  ngOnInit() {
    this.userService.getUserPreferredLanguage().then(language => this.currentLanguage = language);
  }

  async selectLanguage(languageCode: string) {
    await this.userService.setUserPreferredLanguage(languageCode);
    // navigate to next page
    this.router.navigate(['/', 'welcome', 'walkthrough']);
  }

  getButtonStatus(languageCode: string) {
    if (!this.currentLanguage) {
      return 'light';
    }

    if (this.translate.currentLang === languageCode) {
      return 'primary';
    }
    return 'light';
  }

}
