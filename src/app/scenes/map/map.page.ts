import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, from, Subscription } from 'rxjs';
import { ScriptInjectorService } from '../../core/script-injector.service';
import { environment } from '../../../environments/environment';
import { debounceTime, distinct, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';
import { UserService } from 'src/app/core/user.service';
import { GeoPoint } from 'src/app/models/geo-point';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { LoggerService } from 'src/app/core/logger.service';
import { TruckService } from 'src/app/core/truck.service';
import { Truck } from 'src/app/models/interfaces';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit, OnDestroy {
  @ViewChild('mapContainer', { static: false })
  mapContainer: ElementRef;

  subscriptions: Subscription[] = [];
  mapsEventListeners: google.maps.MapsEventListener[] = [];

  map: google.maps.Map;
  trucks: { [key: string]: { marker: google.maps.Marker, truck: Truck } } = {};

  mapBoundsChanged = new BehaviorSubject<google.maps.LatLngBounds>(null);

  constructor(
    private scriptInjector: ScriptInjectorService,
    private userService: UserService,
    private toastController: ToastController,
    private translate: TranslateService,
    private truckService: TruckService,
    private logger: LoggerService) { }

  ngOnInit() {
    this.subscriptions.push(
      this.scriptInjector.loadScript(`https://maps.googleapis.com/maps/api/js?key=${environment.googleMapsAPIKey}&libraries=geometry`).pipe(
        switchMap(() => {
          return from(this.userService.getUserLocation());
        }),
      ).subscribe(async (location) => {
        this.initializeMap(location);
        if (!location) {
          // show the notification
          this.logger.info('Showing the toast message');
          const message = await this.translate.get('map.residential_location_not_available').toPromise();
          const toast = await this.toastController.create({
            message,
            duration: 3000,
            position: 'top'
          });
          await toast.present();
        }
      }),
    );

    this.subscriptions.push(
      this.mapBoundsChanged.pipe(
        filter(bounds => !!bounds),
        debounceTime(100),
        distinctUntilChanged(),
        switchMap(bounds => {
          const northEast: GeoPoint = {
            latitude: bounds.getNorthEast().lat(),
            longitude: bounds.getNorthEast().lng(),
          };
          const southWest: GeoPoint = {
            latitude: bounds.getSouthWest().lat(),
            longitude: bounds.getSouthWest().lng(),
          };

          return this.truckService.getTrucksInViewport(northEast, southWest);
        }),
      ).subscribe((trucks) => {
        this.updateTrucks(trucks);
      }),
    );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }

    for (const listener of this.mapsEventListeners) {
      if (listener) {
        listener.remove();
      }
    }

    this.map = null;
  }

  initializeMap(location: GeoPoint) {
    let center = new google.maps.LatLng(0, 0);
    if (location) {
      center = new google.maps.LatLng(location.latitude, location.longitude);
    }

    this.map = new google.maps.Map(this.mapContainer.nativeElement, {
      center,
      streetViewControl: false,
      mapTypeControl: false,
      clickableIcons: false,
      rotateControl: false,
      fullscreenControl: false,
      minZoom: 12,
      zoom: 15,
    });

    this.mapsEventListeners.push(
      this.map.addListener('bounds_changed', () => this.mapBoundsChanged.next(this.map.getBounds())),
    );
  }

  updateTrucks(trucks: Truck[]) {
    // remove the trucks that are must note be displayed anymore
    const newTrucksMap: { [key: string]: boolean } = {};
    for (const truck of trucks) { newTrucksMap[truck.truckId] = true; }

    for (const truckId in this.trucks) {
      if (!truckId) {
        continue;
      }
      if (newTrucksMap[truckId]) {
        continue;
      }

      // truck must be removed
      this.trucks[truckId].marker.setMap(null);
      delete this.trucks[truckId];
    }

    // add or update the trucks to reflect new positions
    for (const truck of trucks) {
      if (this.trucks[truck.truckId]) {
        // already existing. update the position
        this.logger.info('Alreading existing truck. Updating the marker position', truck);
        const subject = this.trucks[truck.truckId];

        const prevLocation = subject.marker.getPosition();
        const newLocation = new google.maps.LatLng(truck.lastKnownLocation.latitude, truck.lastKnownLocation.longitude);

        subject.truck = truck;
        subject.marker.setPosition(newLocation);
        this.updateTruckMarkerIcon(prevLocation, newLocation, subject.marker);
      } else {
        // new truck. add it to the map
        this.logger.info('New truck. Adding to the map', truck);
        const marker = new google.maps.Marker({
          position: new google.maps.LatLng(truck.lastKnownLocation.latitude, truck.lastKnownLocation.longitude),
        });

        this.updateTruckMarkerIcon(null, marker.getPosition(), marker);

        const subject = {
          truck,
          marker,
        };
        this.trucks[truck.truckId] = subject;
        marker.setMap(this.map);
      }
    }
  }

  updateTruckMarkerIcon(prevLocation: google.maps.LatLng, newLocation: google.maps.LatLng, marker: google.maps.Marker) {
    // set the marker icon based on the direction of the truck
    let rotation = 0;
    if (prevLocation) {
      rotation = google.maps.geometry.spherical.computeHeading(prevLocation, newLocation);
    }
    this.logger.info('Rotation calculated', rotation);
    let imageUrl = 'assets/truck-marker/truck-north.png';

    if (rotation > -45 && rotation <= 45) {
      // north
    } else if (rotation > 45 && rotation <= 135) {
      // east
      imageUrl = imageUrl.replace('north', 'east');
    } else if (rotation > 135 && rotation <= 225) {
      // south
      imageUrl = imageUrl.replace('north', 'south');
    } else {
      // west
      imageUrl = imageUrl.replace('north', 'west');
    }
    marker.setIcon({
      url: imageUrl,
      anchor: new google.maps.Point(32, 32),
      size: new google.maps.Size(64, 64),
      scaledSize: new google.maps.Size(32, 32),
    });
  }
}
