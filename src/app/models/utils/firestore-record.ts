import * as firebase from 'firebase/app';
import 'firebase/firestore';

export type FirestoreRecord<T> = Omit<Omit<T, 'createdAt'>, 'updatedAt'> & {
  createdAt: firebase.firestore.Timestamp;
  updatedAt: firebase.firestore.Timestamp;
};
