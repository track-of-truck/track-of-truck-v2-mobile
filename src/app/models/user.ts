import { GeoPoint } from './geo-point';

export interface User {
  userId: string;
  location?: GeoPoint;
  language?: string;
  notifications: boolean;
}
