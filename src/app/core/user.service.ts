import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { GeoPoint } from '../models/geo-point';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private storage: Storage, private logger: LoggerService, private translate: TranslateService) { }

  getAvailableLanguages() {
    return ['en', 'si', 'ta'];
  }

  async getDefaultLanguage() {
    return 'en';
  }

  async getUserPreferredLanguage() {
    const language = await this.storage.get('language');
    if (language) { return language; }
    return null;
  }

  async setUserPreferredLanguage(languageCode: string) {
    this.logger.info('Setting the user preferred language', languageCode);

    await this.translate.use(languageCode).toPromise();
    return this.storage.set('language', languageCode);
  }

  async getUserLocation() {
    const location: GeoPoint = {
      latitude: 7.8731,
      longitude: 80.7718,
    };
    return of(location).pipe(delay(1000)).toPromise();
  }

  async setResidentialLocation(location: GeoPoint) {
    return of(true).pipe(delay(1000)).toPromise();
  }

  async getUserNotificationPreference() {
    return of(true).pipe(delay(1000)).toPromise();
  }

  async setUserNotificationPreference(notifications: boolean) {
    return of(true).pipe(delay(1000)).toPromise();
  }
}
