import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor() { }

  private log(type: string, message: string, ...data: any[]) {
    const now = new Date().toISOString();
    console.log(now, type, message, ...data);
  }

  info(message: string, ...data: any[]) {
    this.log('INFO', message, ...data);
  }

  warn(message: string, ...data: any[]) {
    this.log('WARN', message, ...data);
  }

  error(message: string, ...data: any[]) {
    this.log('ERROR', message, ...data);
  }
}
