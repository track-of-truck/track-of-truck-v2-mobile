import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import 'firebase/firestore';

import { GeoPoint } from '../models/geo-point';
import { Truck } from '../models/interfaces';
import { FirestoreRecord } from '../models/utils';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class TruckService {

  constructor(private db: AngularFirestore, private logger: LoggerService) { }

  getTrucksInViewport(northEast: GeoPoint, southWest: GeoPoint) {
    const expiryTime = new Date(Date.now() - 60 * 60 * 1000); // 10 minutes history
    const expiryTimestamp = firebase.firestore.Timestamp.fromDate(expiryTime);

    const trucksRef = this.db.collection<FirestoreRecord<Truck>>('Trucks', (ref) => ref.where('lastUpdatedTime', '>=', expiryTimestamp));
    return trucksRef.valueChanges().pipe(map(trucks => {
      this.logger.info(`Retrieved ${trucks.length} trucks`);
      return trucks.map(truck => {
        const parsedTruck: Truck = {
          ...truck,
          createdAt: truck.createdAt.toDate(),
          updatedAt: truck.updatedAt.toDate(),
        };
        return parsedTruck;
      });
    }));
  }
}
