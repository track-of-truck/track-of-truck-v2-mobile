import { Injectable } from '@angular/core';
import { ReplaySubject, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ScriptInjectorService {
  private scripts: {
    [source: string]: { source: string; loaded$: ReplaySubject<boolean> };
  } = {};

  constructor() {}

  loadScripts(...sources: string[]) {
    const observables = sources.map((source) => this.loadScript(source));
    return forkJoin(observables);
  }

  loadScript(source: string) {
    if (this.scripts[source]){
      return this.scripts[source].loaded$.asObservable();
    }

    const scriptEntry = {
      source,
      loaded$: new ReplaySubject<boolean>(1),
    };
    this.scripts[source] = scriptEntry;

    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = source;

    script.onload = () => {
      // TODO add support for IE as well
      scriptEntry.loaded$.next(true);
    };

    document.getElementsByTagName('head')[0].appendChild(script);

    return scriptEntry.loaded$.asObservable();
  }
}
