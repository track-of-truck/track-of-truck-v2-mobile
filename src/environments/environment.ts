// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  googleMapsAPIKey: 'AIzaSyBXCAXUvMJ6gVwO6f7E306T9ZaUSM3K4mA',
  firebaseConfig: {
    apiKey: 'AIzaSyBXCAXUvMJ6gVwO6f7E306T9ZaUSM3K4mA',
    authDomain: 'track-of-truck-v2.firebaseapp.com',
    databaseURL: 'https://track-of-truck-v2.firebaseio.com',
    projectId: 'track-of-truck-v2',
    storageBucket: 'track-of-truck-v2.appspot.com',
    messagingSenderId: '129311167191',
    appId: '1:129311167191:web:4aa630bc58acc686b0d692',
    measurementId: 'G-3WSD2X40JX'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
